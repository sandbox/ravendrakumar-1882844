Drupal colorbox module:
------------------------
Maintainers:
  Ravendra (http://drupal.org/user/2426114)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
Twitter Widget with Sentiment Analysis is a really powerful widget that shows tweets and other
mentions from across the web for any give keyword, your handle along with sentiment analysis.  It also starts to show trend lines if there is enough volume.

* SocialAppsHQ Widgets - http://widget.socialappshq.com/widget/landing


Features:
---------

Twitter Widget Module:

* Pulls latest Tweets for any keyword or handle
* Shows trends if there is enough volume
* Shows sentiment analysis for the mentions
* Customize color, font & number of mentions.

The Twitter plugin is:

* Tested in Firefox 2 & 3, Safari 3 & 4, Opera 9, Chrome,
  Internet Explorer 6, 7, 8.
* Released under the MIT License.


Installation:
------------
1. Download and unpack the widget plugin in "sites/all/modules".
2. Download and unpack the twitter widget module directory in your modules folder
   (this will usually be "sites/all/modules/").
3. Go to "Administer" -> "Modules" and enable the module.

Contributions:
--------------
* Initial Build
  by Ravendra (http://drupal.org/user/2426114).

